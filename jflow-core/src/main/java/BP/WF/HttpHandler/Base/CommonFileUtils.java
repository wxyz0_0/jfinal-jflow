package BP.WF.HttpHandler.Base;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.jfinal.upload.MultipartRequest;
import com.jfinal.upload.UploadFile;
public class CommonFileUtils {
	/**
	 * 上传
	 * @param request
	 */
	public static void upload(HttpServletRequest request,String fileName,File targetFile) throws Exception{
		String contentType = request.getContentType();
		if (contentType != null && contentType.indexOf("multipart/form-data") != -1) {
			//JFinal方式
			//注意：务必在这里将request更改为MultipartRequest类型，否则报错
			if (request instanceof MultipartRequest == false)
				request = new MultipartRequest(request);		
			MultipartRequest mrequest = (MultipartRequest)request;

			UploadFile currentUploadFile = null;
			List<UploadFile> uploadFiles = mrequest.getFiles();
			for (UploadFile uploadFile : uploadFiles) {
				if (uploadFile.getParameterName().equals(fileName)) {
					currentUploadFile = uploadFile;
				}
			}
			
			if(currentUploadFile != null){
				try (
						InputStream is = new FileInputStream(currentUploadFile.getFile());	
						FileOutputStream fos = new FileOutputStream(targetFile);
						){

					int buffer = 1024; // 定义缓冲区的大小
					int length = 0;
					byte[] b = new byte[buffer];
					while ((length = is.read(b)) != -1) {
						// 计算上传文件的百分比
						fos.write(b, 0, length); // 向文件输出流写读取的数据
					}
					fos.close();
				} catch (RuntimeException ex) {
					throw new RuntimeException("@文件存储失败,有可能是路径的表达式出问题,导致是非法的路径名称:" + ex.getMessage());
				}
			}
		}
	}
	
	/**
	 * 获取原始的文件名
	 * @param request
	 * @return
	 */
	public static String getOriginalFilename(HttpServletRequest request,String fileName){
		
//		Map<String,Object> res = new HashMap<>();
		
		String contentType = request.getContentType();
		if (contentType != null && contentType.indexOf("multipart/form-data") != -1) {
			//JFinal方式
			//注意：务必在这里将request更改为MultipartRequest类型，否则报错
			if (request instanceof MultipartRequest == false)
				request = new MultipartRequest(request);		
			MultipartRequest mrequest = (MultipartRequest)request;
			
			String originalFileName = null;
			List<UploadFile> uploadFiles = mrequest.getFiles();
			for (UploadFile uploadFile : uploadFiles) {
				if (uploadFile.getParameterName().equals(fileName)) {
					originalFileName = uploadFile.getOriginalFileName();
				}
			}
			
//			res.put("originalFileName", originalFileName);
//			res.put("request", request);
//
//			return res;
			return originalFileName;
			
		}else{
			return null;
		}
	}
	
	/**
	 * 获取上传的文件
	 * @param request
	 * @return
	 */
	public static long getFilesSize(HttpServletRequest request,String fileName){
		
//		Map<String,Object> res = new HashMap<>();
//		
		String contentType = request.getContentType();
		if (contentType != null && contentType.indexOf("multipart/form-data") != -1) {
			//JFinal方式
			//注意：务必在这里将request更改为MultipartRequest类型，否则报错
			if (request instanceof MultipartRequest == false)
				request = new MultipartRequest(request);		
			MultipartRequest mrequest = (MultipartRequest)request;
			
			UploadFile currentUploadFile = null;
			List<UploadFile> uploadFiles = mrequest.getFiles();
			for (UploadFile uploadFile : uploadFiles) {
				if (uploadFile.getParameterName().equals(fileName)) {
					currentUploadFile = uploadFile;
				}
			}
			
//			res.put("fileSize", currentUploadFile.getFile().length());
//			res.put("request", request);
//			return res;
			return currentUploadFile.getFile().length();
		}else{
			return 0;
		}		
	}
	
	/**
	 * 获取文件对于的输入流
	 * @param request
	 * @param fileName
	 * @return
	 * @throws IOException 
	 */
	public static InputStream getInputStream(HttpServletRequest request,String fileName) throws IOException{
		
//		Map<String,Object> res = new HashMap<>();
		
		String contentType = request.getContentType();
		if (contentType != null && contentType.indexOf("multipart/form-data") != -1) {
			//JFinal方式
			//注意：务必在这里将request更改为MultipartRequest类型，否则报错
			if (request instanceof MultipartRequest == false)
				request = new MultipartRequest(request);		
			MultipartRequest mrequest = (MultipartRequest)request;
			
			UploadFile currentUploadFile = null;
			List<UploadFile> uploadFiles = mrequest.getFiles();
			for (UploadFile uploadFile : uploadFiles) {
				if (uploadFile.getParameterName().equals(fileName)) {
					currentUploadFile = uploadFile;
				}
			}
			
			if(currentUploadFile != null){
				return new FileInputStream(currentUploadFile.getFile());
			}else{
				return null;
			}
			
//			res.put("inputStream", new FileInputStream(currentUploadFile.getFile()));
//			res.put("request", request);		
//			return res;
		}else{
			return null;
		}		
	}
}
