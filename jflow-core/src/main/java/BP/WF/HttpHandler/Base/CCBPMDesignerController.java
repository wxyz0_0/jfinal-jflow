package BP.WF.HttpHandler.Base;

import javax.servlet.http.HttpServletRequest;

import BP.WF.HttpHandler.WF_Admin_CCBPMDesigner;


public class CCBPMDesignerController extends HttpHandlerBase {
	
	public CCBPMDesignerController(){
		System.out.println("CCBPMDesignerController 初始化");
	}
	
	/**
	 * jfinal 默认执行的方法
	 */
	public void index() {
		System.out.println("CCBPMDesignerController  index()");
		this.ProcessRequestPost(this.getRequest());
	}
	
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
//	@RequestMapping(value = "/ProcessRequest")
//	public final void ProcessRequestPost() 
//	{
//		WF_Admin_CCBPMDesigner  CCBPMDHandler = new WF_Admin_CCBPMDesigner();
//		super.ProcessRequest(CCBPMDHandler);
//	}
	
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	public final void ProcessRequestPost(HttpServletRequest request) {
		System.out.println("CCBPMDesignerController  ProcessRequestPost()");
		WF_Admin_CCBPMDesigner CommHandler = new WF_Admin_CCBPMDesigner();
		
		//调试用日志
//		String temp = request.getRequestURL().toString();
//		System.out.println(temp);
//
//		Enumeration enump = request.getParameterNames();
//		while(enump.hasMoreElements()){
//			String name = enump.nextElement().toString();
//			System.out.println(name + ":" + request.getParameter(name));
//		}
		
		//JFinal方式返回JSON数据
		String data = super.getProcessRequestResult(CommHandler);
		System.out.println(data);
		this.renderJson(data);
	}
	
	@Override
	public Class <WF_Admin_CCBPMDesigner>getCtrlType() {
		return WF_Admin_CCBPMDesigner.class;
	}

}
