package BP.JFINAL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.upload.MultipartRequest;

/**
 * 请求过滤器
 * @author asus
 *
 */
public class RequestHander extends Handler {
	
	static final ThreadLocal<HttpServletRequest> tlRequest = new ThreadLocal<>();
	static final ThreadLocal<HttpServletResponse> tlResponse = new ThreadLocal<>();
	
	public RequestHander() {
	}

	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		
		String contentType = request.getContentType();
		if (contentType != null && contentType.indexOf("multipart/form-data") != -1) {
			if (request instanceof MultipartRequest == false)
				request = new MultipartRequest(request);	
		}
		tlRequest.set(request);
		tlResponse.set(response);
       try {
    	   next.handle(target, request, response, isHandled);
       } finally {
    	   tlRequest.remove();
    	   tlResponse.remove(); 
       }
	}
	
    public static HttpServletRequest getRequest() {
        return tlRequest.get();
    }
    
    public static HttpServletResponse getResponse() {
        return tlResponse.get();
    }
}
